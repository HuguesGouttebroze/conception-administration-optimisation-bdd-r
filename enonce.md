# Conception, administration & optimisation BDD relationnelles
## Contexte

* La société « Délivré-où ? » est spécialisée dans la livraison de plats cuisinés à domicile. Pour cela, elle fait appel à des auto-entrepreneurs dont le sérieux laisse parfois à désirer (retards, non-livraison, etc.).

* « Délivré-où ? » propose à ses clients un site web et une application mobile pour réaliser des commandes, et souhaite garder une trace des informations afin de faire un suivi qualité.
    
    - Vous êtes chargés de développer le prototype de cette base de données.

    « Délivré-où ? » travaille avec de nombreux restaurants, dont on connait la raison sociale et le délai de livraison habituel. 
    
    Exemples : 
    Chamas Tacos (15 minutes), 
    Salmon Lovers (30 minutes), 
    La Pizza
    Lyonnaise (20 minutes), 
    Cousbox (25 minutes), 
    Burger House (10 minutes), 
    Crok’in (15 minutes), 
    etc.

    Chaque restaurant se situe à une adresse postale précise.
    Les clients souhaitant passer une commande peuvent s’inscrire en utilisant le formulaire d’inscription de l’application mobile ou du site web de « Délivré-où ? ». Ils doivent indiquer :

        ▪ Leur prénom ;
        ▪ Leur nom ;
        ▪ Leur adresse e-mail (composée d’un @ suivi d’un point) ;
        ▪ Leur mot de passe (qui sera haché en SHA-256, sur 64 caractères) ;
        ▪ Leur numéro de téléphone portable (au format français, sans indicatif, sans formatage).

- Note : dans ce formulaire d’inscription, les clients n’indiquent pas leur adresse postale.
        
        Les coursiers souhaitant transporter les commandes des clients doivent également s’inscrire enutilisant ce même formulaire. Contrairement aux clients, ils doivent  indiquer leur numéro de SIRET (sur exactement 14 caractères).

        * Depuis l’application mobile et/ou le site web de« Délivré-où ? », le client peut passer autant de commande qu’il le souhaite. Au moment où il passe commande, le client doit indiquer :

            ▪ Le restaurant chargé de préparer sa commande ;
            ▪ Les consignes de livraison (texte facultatif de 200
            caractère maximum) ;
            ▪ L’adresse postale à laquelle il souhaite que la
            commande soit acheminée.

    Note : À ce stade, la base de données ne doit pas gérer le détail des commandes (pas de paniers, pas d’articles, pas de quantité, pas de prix unitaire).

    Lorsque le client clique sur « Passer la commande », la date et l’heure sont mémorisés au niveau de commande. 

    C’est à ce moment qu’un coursier est affecté à la commande par le système (c’est donc lui qui sera chargé de la transporter).

    Un logiciel interne à « Délivré-où ? », exécuté par tâche planifiée, doit pouvoir inscrire dans la base de données la date et l’heure à laquelle la commande sera préparée par le restaurant ainsi que la date et l’heure de livraison estimée de la commande. 
    
    Plus tard, lorsque le coursier se présente pour livrer la commande au client, il clôture la commande depuis l’application mobile « Délivré-où ? ». C’est à ce moment qu’est mémorisée la date et l’heure de livraison réelle de la commande.

* Précisions importantes :

    ▪ Une fonctionnalité importante devra être implémentée dans l’application mobile et le site web de « Délivré-où ? » : il devra être possible, pour un client, de bloquer un ou plusieurs coursiers, de manière à ce que ce coursier ne soit plus affecté aux prochaines commandes de ce client. Le client peut préciser un motif pour ce blocage (sur 100 caractères).

    ▪ Un coursier peut tout à fait être également client, et à ce titre, effectuer des commandes.

    ▪ La base de données sera utilisée pour calculer le nombre de minutes de retard au niveau des commandes.

# EXAMEN

## A. Concevez la base de données – 8 points – 55 minutes :

IL EST CONSEILLÉ DE LIRE L’ÉNONCÉ ENTIÈREMENT (PARTIE A, B, C, D, E ET F COMPRISES).

* Construisez le MCD à l’aide du logiciel Looping :

    ➢ Pour nommer les entités et les attributs, n’utilisez ni espace ni accent
    ➢ Sauf cas particulier, la taille des valeurs alphanumériques sera de 50
    ➢ Identifiez vos entités ainsi : « idEntite » (où « Entite » est le nom de l’entité)
    ➢ N’utilisez pas d’AUTO-INCREMENT sur les tables utilisées dans un héritage

LORSQUE VOUS AVEZ TERMINÉ, DÉPOSEZ VOTRE MCD SUR MOODLE
ET ATTENDEZ LE CORRIGÉ DU FORMATEUR, QUE VOUS UTILISEREZ DANS LA SUITE DE L’ÉNONCÉ.

## B. Mettez en place la base de données – 3 points – 15 minutes :

1. À l’aide du fichier « .loo » corrigé, générez le script SQL

2. À l’aide de PhpMyAdmin, appliquez le script SQL dans une nouvelle base nommée « DelivreouPrenom » (où « Prenom » est votre prénom, sans accent, sans tiret, sans espace)

3. À l’aide de PhpMyAdmin, ajoutez une colonne « montant » de type « DECIMAL(10,2) » n’autorisant pas les valeurs NULL dans la table « Commande »

4. À l’aide de PhpMyAdmin, renommez les colonnes « idIndividu » & « idIndividu_1 » en « idIndividuCoursier » & « idIndividuClient » dans les tables « Bloquer » ET « Commande »

## C. Rédigez les requêtes ci-dessous (alimentation de données) – 4 points – 40 minutes :

SAUVEGARDEZ LES REQUÊTES SQL DANS UN FICHIER TEXTE AU FUR ET À MESURE QUE VOUS LES RÉDIGEZ.

1. Lisez, puis exécutez le script SQL fourni sur Moodle. Il insère certaines données de bases.

2. Insérez un client correspondant à vous-même en utilisant l’id « 21 ». 

Pour le mot de passe, utilisez : SHA2(CONV(FLOOR(RAND() * 99999999999999), 20, 36), 256)

3. Insérez un coursier correspondant à un personnage de fiction de votre choix en utilisant l’id « 22 » en en inventant les autres valeurs.

4. Insérez, en une seule requête, les 6 restaurants mentionnés dans l’énoncé, en les rattachant à une adresse de votre choix.

5. Créez une procédure stockée qui ajoute aléatoirement entre 60 et 100 commandes répondant aux critères ci-dessous, et exécutez-là :
▪ Le montant total sera un nombre décimal compris entre 8 et 70 €
▪ La consigne sera une suite de caractère aléatoire
▪ La date de commande sera aléatoirement comprise entre le 26 novembre 2020 et aujourd’hui. Utilisez : NOW() - INTERVAL RAND() * 21780 MINUTE
▪ La date de préparation terminée sera aléatoirement comprise entre la date de
commande + 5 minutes et la date de commande + 35 minutes
▪ La date de livraison estimée sera comprise entre la date de préparation terminée + 5
minutes et la date de préparation terminée + 25 minutes
▪ La date de livraison réelle sera elle aussi comprise entre la date de préparation
terminée + 5 minutes et la date de préparation terminée + 25 minutes
▪ Les différentes clés étrangères seront sélectionnées au hasard dans les tables
correspondantes
